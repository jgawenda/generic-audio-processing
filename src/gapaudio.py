from scipy.io import wavfile
from scipy import signal
from math import floor
import numpy
import wave

# class containing IIR filter's data
    # ------ DATA ------
    # gap_sos - a SOS matrix for filtering purposes

class GapFilter:
    # generate_filter(filter_order, Wn, fs, filter_type)
    # constructs a SOS matrix for filtering purposes when initialized
    # uses a Butterworth's filter
    # ------ ARGUMENTS ------
    # filter_order - ammount of coefficients in the filter (bigger order = better filtration at cutoff frequency, takes longer
    # Wn - in lowpass/highpass filter = a scalar determining cutoff frequency  
    #    - in bandpass/bandstop filter = a vector of two frequencies [fl, fh], fl < fh
    # fs - sampling frequency
    # filter_type = {'lowpass', 'highpass', 'bandpass', 'bandstop'} - determines filter's type
    def generate_filter(self, filter_order, Wn, fs, filter_type):
        if (type(Wn) is list):
            if (len(Wn) == 2):
                Wn[0] = Wn[0]/fs
                Wn[1] = Wn[1]/fs

        else:
            Wn = Wn/fs

        self.gap_sos = signal.butter(filter_order,
                                     Wn,
                                     filter_type,
                                     output='sos',
                                     )

    # print_info()
    # function for printing filter's info
    def print_info(self):
        print(self.gap_sos)


# class containing audio data and information of a file
    # ------ DATA ------
    # gap_filename - name of processed file
    # gap_channels - the number of audio channels
    # gap_nframes - the number of audio frames in the file
    # gap_sampwidth - sample width in bytes
    # gap_fs - sampling frequency in Hz
    # gap_data - audio data from the file

class GapAudio:
    # read_file(filename) 
    # gets information about audio file and initializes a GapAudio object
    # ------ ARGUMENTS ------
    # filename - name of processed file
    def read_file(self, filename):
        # writing information into the class
        self.gap_filename = filename # filename of a file to work on

        with wave.open(filename) as file:
            self.gap_channels = file.getnchannels()
            self.gap_nframes = file.getnframes()
            self.gap_sampwidth = file.getsampwidth()

        self.gap_fs, self.gap_data = wavfile.read(filename)

    # prints info about the file
    def print_info(self):
        print("Filename:", self.gap_filename)
        print("Sampling frequency:", self.gap_fs, "Hz")
        print("Audio channels:", self.gap_channels)
        print("Sample width:", self.gap_sampwidth, "bytes")

    # filter_data(gap_filter)
    # filters audio data depending on given filter
    # ------ ARGUMENTS ------
    # gap_filter - a GapFilter object containing SOS matrix 
    def filter_data(self, gap_filter):

        # if audio is mono, the function filters given data
        if (self.gap_channels == 1):
            new_data = signal.sosfilt(gap_filter.gap_sos,
                                      self.gap_data,
                                      )
            self.gap_filtered_data = numpy.asarray(new_data,
                                          dtype=numpy.int16,
                                          )

        # if audio is stereo, the function takes left and right channels and filters them seperately
        else:
            data_left = self.gap_data[:, 0]
            data_right = self.gap_data[:, 1]

            # filtration of left and right channels
            new_data_left = signal.sosfilt(gap_filter.gap_sos,
                                           data_left,
                                           )
            new_data_right = signal.sosfilt(gap_filter.gap_sos,
                                            data_right,
                                            )

            # saving audio data as a two dimensional array
            self.gap_filtered_data = numpy.column_stack((new_data_left, new_data_right))
            self.gap_filtered_data = numpy.asarray(self.gap_filtered_data,
                                          dtype=numpy.int16,
                                          )
    
    # change_volume(percent_change)
    # method for changing audio volume of the file
    # ------ ARGUMENTS ------
    # percent_change - signed int/float representing change of volume
    # -50 equals to 1/2 of original volume
    # 50 equals to 1.5 of original volume
    def change_volume(self, percent_change):
        new_data = []
        if (percent_change < 0):
            for sample in self.gap_data:
                new_data.append(floor(sample * (1 - abs(percent_change)/100)))

        else:
            for sample in self.gap_data:
                new_data.append(floor(sample * (1 + percent_change/100)))

        self.gap_filtered_data = numpy.asarray(new_data, dtype=numpy.int16)

    # remove_voices()
    # EXPERIMENTAL
    # inverts audio track from one channel then mixes both channels to mono
    # doesn't work for every song, just those which have vocal track mixed both in left and right channel
    def remove_voices(self):
        if (self.gap_channels == 2):
            data_left = self.gap_data[:, 0]
            data_right = self.gap_data[:, 1]

            data_right = numpy.invert(data_right)

            self.gap_filtered_data = numpy.asarray((data_left + data_right),
                                          dtype=numpy.int16,
                                          )

    def get_fs(self):
        return self.gap_fs

    def get_channels(self):
        return self.gap_channels

    def get_data(self):
        return self.gap_data

    def get_filtered_data(self):
        return self.gap_filtered_data
