from tkinter import *
from tkinter import filedialog
from gapaudio import GapAudio
from gapaudio import GapFilter
from scipy.io import wavfile

# event for handling b_file_dialog click
# opens a file selection menu and reads data to GapAudio object - audio_data
def open_file_action():
    file_name = filedialog.askopenfilename(initialdir='.',
                                       title = "Select a WAV file",
                                       filetypes = (("WAV files", ".wav"),))

    get_audio_data(file_name)

    e_audio_name.config(state = NORMAL)
    e_audio_name.delete(0, END)
    e_audio_name.insert(0, file_name)
    e_audio_name.config(state = "readonly")

    l_audio_fs.config(text="Sampling frequency: " + str(audio_data.get_fs()) + " Hz",
                      anchor = W,)
    l_audio_channels.config(text="Audio channels: " + str(audio_data.get_channels()),
                            anchor = W,)

    return file_name

# event for handling b_save_dialog click
# opens a file selection menu, filters audio data and saves it to given file
# gets data from w_filter_menu, e_filter_order, e_low_freq, e_high_freq
#   w_filter_menu - contains filter types {'lowpass', 'highpass', 'bandpass', 'bandstop')
#   e_filter_order - contains an integer describing filter's order
#   e_low_freq, e_high_freq - contain low and high cutoff frequencies
def save_file_action():
    # getting an absolute path to save a file
    save_name = filedialog.asksaveasfilename(initialdir='.',
                                           title = "Save a WAV file",
                                           filetypes = (("WAV files", ".wav"),))

    # getting filter's type and filter's order
    filter_type = v_filter.get()
    filter_order = int(v_filter_order.get())

    # filtrating data depends on given type
    if (filter_type == "lowpass"):
        gap_filter.generate_filter(filter_order,
                                   float(v_low_freq.get()),
                                   audio_data.get_fs(),
                                   filter_type)

        audio_data.filter_data(gap_filter)

        wavfile.write(save_name,
                      audio_data.get_fs(),
                      audio_data.get_filtered_data())

    elif (filter_type == "highpass"):
        gap_filter.generate_filter(filter_order,
                                   float(v_high_freq.get()),
                                   audio_data.get_fs(),
                                   filter_type)

        audio_data.filter_data(gap_filter)

        wavfile.write(save_name,
                      audio_data.get_fs(),
                      audio_data.get_filtered_data())

    else:
        gap_filter.generate_filter(filter_order,
                                   [float(v_low_freq.get()), float(v_high_freq.get())],
                                   audio_data.get_fs(),
                                   filter_type)

        audio_data.filter_data(gap_filter)

        wavfile.write(save_name,
                      audio_data.get_fs(),
                      audio_data.get_filtered_data())

# changes state of entry boxes from normal to readonly and vice versa
# used to emphasize which frequency will be applied to the filter
# e.g. for a 'lowpass' filter, "High cutoff frequency" will be grayed out
def event_filter_change(*args):
    if (v_filter.get() == "lowpass"):

        e_low_freq.config(state = NORMAL)
        e_high_freq.config(state = "readonly")

    elif (v_filter.get() == "highpass"):
        e_high_freq.config(state = NORMAL)
        e_low_freq.config(state = "readonly")

    else:
        e_high_freq.config(state = NORMAL)
        e_low_freq.config(state = NORMAL)

# reads audio data from given file
def get_audio_data(file_name):
    audio_data.read_file(file_name)

# barebones error handling
def event_low_freq_change(*args):
    try:
        test_var = float(v_low_freq.get())
    except ValueError:
        b_save_dialog.config(state = DISABLED)
    else:
        if ((float(v_low_freq.get()) > float(v_high_freq.get())) and v_filter.get() in ["bandpass", "bandstop"]):
            b_save_dialog.config(state = DISABLED)
        else:
            b_save_dialog.config(state = NORMAL)

def event_high_freq_change(*args):
    try:
        test_var = float(v_high_freq.get())
    except ValueError:
        b_save_dialog.config(state = DISABLED)
    else:
        if ((float(v_low_freq.get()) > float(v_high_freq.get())) and v_filter.get() in ["bandpass", "bandstop"]):
            b_save_dialog.config(state = DISABLED)
        else:
            b_save_dialog.config(state = NORMAL)

def event_filter_order_change(*args):
    try:
        test_var = int(v_filter_order.get())
    except ValueError:
        b_save_dialog.config(state = DISABLED)
    else:
        if (int(v_filter_order.get()) < 0):
            b_save_dialog.config(state = DISABLED)
        elif (int(v_filter_order.get()) == 0):
            b_save_dialog.config(state = DISABLED)
        else:
            b_save_dialog.config(state = NORMAL)


# creating and initializing main window for the application
root = Tk()

# ------ DEFAULTS AND VARIABLES ------
audio_data = GapAudio() # an object containing data (and filtered data) of given audio file
gap_filter = GapFilter() # an object containing data of a created filter
v_filter_options = ["lowpass",
                  "highpass",
                  "bandpass",
                  "bandstop"] # a list containing available filter types

v_low_freq = StringVar(root)
v_low_freq.set("200")
v_high_freq = StringVar(root)
v_high_freq.set("3000")
v_filter_order = StringVar(root)
v_filter_order.set("4")
v_filter = StringVar(root) # string variable to be used with w_filter_menu OptionMenu object
v_filter.set(v_filter_options[0]) # setting a default

# creating a button for opening files
b_file_dialog = Button(root,
                       text=" ... ",
                       command=open_file_action)


l_audio_name = Label(root,
                     text = "Selected audio file",).grid(row=0, columnspan=4)
e_audio_name = Entry(root,
                     width = 60,
                     borderwidth = 4,
                     state = "readonly",)

# a box containing an absolute path to the file
e_audio_name.grid(row = 1, columnspan = 5)
b_file_dialog.grid(row = 1, column = 6)

# creating text which prints audio file's number of channels and its sampling frequency
l_audio_fs = Label(root,
                   text = "Sampling frequency: ",
                   anchor = W,
                   justify = CENTER)
l_audio_channels = Label(root,
                         text = "Audio channels: ",
                         anchor = W,
                         justify = CENTER,)
l_audio_fs.grid(row = 2, columnspan = 4)
l_audio_channels.grid(row = 3, columnspan = 4)

# a dropdown menu for selecting the type of a filter
l_filter_menu = Label(root,
                      text = "Filter type",
                      justify = "center",).grid(row = 4, column = 1)
w_filter_menu = OptionMenu(root, v_filter, *v_filter_options)
w_filter_menu.grid(row = 5, column = 1)

# an entry box for inputting the order of a filter
l_filter_order = Label(root,
                       text = "Filter order",
                       justify = "center",).grid(row = 4, column = 2)
e_filter_order = Entry(root,
                       width = 10,
                       borderwidth = 4,
                       textvariable = v_filter_order)
e_filter_order.grid(row = 5, column = 2)

# an entry box for inputting low cutoff frequency
l_low_freq = Label(root,
                   text = "Low cutoff frequency (Hz)",
                   justify = "center",).grid(row = 4, column = 3)
e_low_freq = Entry(root,
                   width = 20,
                   borderwidth = 4,
                   textvariable = v_low_freq)
e_low_freq.grid(row = 5, column = 3)

# an entry box for inputting high cutoff frequency
l_high_freq = Label(root,
                    text = "High cutoff frequency (Hz)",
                    justify = "center",).grid(row = 6, column = 3)
e_high_freq = Entry(root,
                    width = 20,
                    borderwidth = 4,
                    textvariable = v_high_freq,
                    state = "readonly",)
e_high_freq.grid(row = 7, column = 3)

# empty label for visual purposes
l_empty = Label(root,
                text = " ",).grid(row = 9, column = 3)

# dialog for saving and filtering data
b_save_dialog = Button(root,
                       text = "Filter & Save",
                       command = save_file_action)
b_save_dialog.grid(row = 10, column = 1)

b_close_window = Button(root,
                        text = "Exit",
                        command = root.destroy,
                        )
b_close_window.grid(row = 10, column = 3)

v_filter.trace('w', event_filter_change) # watching a variable for any changes
v_low_freq.trace('w', event_low_freq_change)
v_high_freq.trace('w', event_high_freq_change)
v_filter_order.trace('w', event_filter_order_change)

root.mainloop()
